#!/bin/bash
set -eu -o pipefail

folder="/etc/docker/compose/app"

# Get tags from Azure metadata endpoint
tags=$(curl -s -H Metadata:true --noproxy "*" "http://169.254.169.254/metadata/instance/compute/tags?api-version=2020-06-01&format=text")
IFS=';' read -ra ADDR <<< "$tags"
for i in "${ADDR[@]}"; do
    set -- `echo $i | sed 's/:/ /'`
    if [ "repository" = "$1" ]; then
        repository="$2"
    fi
done

# Check if the folder is empty
if [ -z "$(ls -A $folder)" ]; then
    echo "Cloning $repository ..."
    git clone --recurse-submodules $repository $folder
else
    echo "docker-compose folder already initialized"
fi
