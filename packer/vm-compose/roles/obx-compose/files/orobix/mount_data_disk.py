import os, re
from time import sleep
import azurelinuxagent.common.logger as logger
from azurelinuxagent.common.osutil import get_osutil
from azurelinuxagent.common.osutil.default import DefaultOSUtil
import azurelinuxagent.common.utils.shellutil as shellutil
import azurelinuxagent.common.utils.fileutil as fileutil
from azurelinuxagent.daemon.resourcedisk.default import ResourceDiskHandler

AZURE_DISK_PATH = "/dev/disk/azure"
DATA_DISK_LUN = 10
MOUNT_POINT = "/data"
MOUNT_OPTIONS = "defaults,nofail"
FS = "ext4"

DATALOSS_WARNING_FILE_NAME = "OROBIX_DATA_DISK_README.txt"
DATA_LOSS_WARNING = """\
THIS IS A DATA DISK.
Data stored in this data disk is NOT DELETED whenever the virtual machine is reprovisioned.
"""


class DataDiskHandler(ResourceDiskHandler):

    def __init__(self):
        self.osutil = get_osutil()
        self.fs = FS
        self.lun = DATA_DISK_LUN
        logger.add_logger_appender(logger.AppenderType.CONSOLE, logger.LogLevel.INFO, path="/dev/stdout")
    
    def run(self):
        mount = self.activate_data_disk()
        print("Mounted {}".format(mount))

    def activate_data_disk(self):
        logger.info("Activate resource disk")
        try:
            mount_point = MOUNT_POINT
            mount_point = self.mount_data_disk(mount_point)
            warning_file = os.path.join(mount_point,
                                        DATALOSS_WARNING_FILE_NAME)
            try:
                fileutil.write_file(warning_file, DATA_LOSS_WARNING)
            except IOError as e:
                logger.warn("Failed to write data loss warning:{0}", e)
            return mount_point
        except Exception as e:
            logger.error("Failed to mount resource disk {0}", e)

    def mount_data_disk(self, mount_point):
        device = self.device_for_lun('lun{}'.format(self.lun))
        if device is None:
                raise Exception("unable to detect disk topology")

        partition = device + "1"
        mount_list = shellutil.run_get_output("mount")[1]
        existing = self.osutil.get_mount_point(mount_list, device)

        if existing:
            logger.info("Data disk [{0}] is already mounted [{1}]",
                        partition,
                        existing)
            return existing

        try:
            fileutil.mkdir(mount_point, mode=0o755)
        except OSError as ose:
            msg = "Failed to create mount point " \
                    "directory [{0}]: {1}".format(mount_point, ose)
            logger.error(msg)
            raise Exception(ose)

        logger.info("Examining partition table")
        ret = shellutil.run_get_output("parted --script {0} print".format(device))
        if ret[0]:
            logger.info("Could not determine partition info for {0}".format(device))
            ret = shellutil.run_get_output("parted --script -a optimal {0} mklabel gpt mkpart primary {1} 0% 100%".format(device, self.fs))
            logger.info("Created GPT partition table for {0}".format(device))

        ret = shellutil.run_get_output("parted --script {0} print".format(device))
        if ret[0]:
            raise Exception("Could not determine partition info for {0}: {1}".format(device, ret[1]))

        force_option = 'F'
        if FS == 'xfs':
            force_option = 'f'
        mkfs_string = "mkfs.{0} -{2} {1}".format(
            FS, partition, force_option)


        if "gpt" in ret[1]:
            logger.info("GPT detected, finding partitions")
            parts = [x for x in ret[1].split("\n") if
                        re.match(r"^\s*[0-9]+", x)]
            logger.info("Found {0} GPT partition(s).", len(parts))
            if len(parts) > 1:
                logger.info("Removing old GPT partitions")
                for i in range(1, len(parts) + 1):
                    logger.info("Remove partition {0}", i)
                    shellutil.run("parted {0} rm {1}".format(device, i))

                logger.info("Creating new GPT partition")
                shellutil.run(
                    "parted {0} mkpart primary 0% 100%".format(device))

                logger.info("Format partition [{0}]", mkfs_string)
                shellutil.run(mkfs_string)
        else:
            logger.info("GPT not detected, determining filesystem")
            ret = self.change_partition_type(
                suppress_message=True,
                option_str="{0} 1 -n".format(device))
            ptype = ret[1].strip()
            if ptype == "7" and self.fs != "ntfs":
                logger.info("The partition is formatted with ntfs, updating "
                            "partition type to 83")
                self.change_partition_type(
                    suppress_message=False,
                    option_str="{0} 1 83".format(device))
                self.reread_partition_table(device)
                logger.info("Format partition [{0}]", mkfs_string)
                shellutil.run(mkfs_string)
            else:
                logger.info("The partition type is {0}", ptype)

        mount_options = MOUNT_OPTIONS
        mount_string = self.get_mount_string(mount_options,
                                                partition,
                                                mount_point)
        attempts = 5
        while not os.path.exists(partition) and attempts > 0:
            logger.info("Waiting for partition [{0}], {1} attempts remaining",
                        partition,
                        attempts)
            sleep(5)
            attempts -= 1

        if not os.path.exists(partition):
            raise Exception(
                "Partition was not created [{0}]".format(partition))

        logger.info("Mount resource disk [{0}]", mount_string)
        ret, output = shellutil.run_get_output(mount_string, chk_err=False)
        # if the exit code is 32, then the resource disk can be already mounted
        if ret == 32 and output.find("is already mounted") != -1:
            logger.warn("Could not mount resource disk: {0}", output)
        elif ret != 0:
            # Some kernels seem to issue an async partition re-read after a
            # 'parted' command invocation. This causes mount to fail if the
            # partition re-read is not complete by the time mount is
            # attempted. Seen in CentOS 7.2. Force a sequential re-read of
            # the partition and try mounting.
            logger.warn("Failed to mount resource disk. "
                        "Retry mounting after re-reading partition info.")

            self.reread_partition_table(device)

            ret, output = shellutil.run_get_output(mount_string, chk_err=False)
            if ret:
                logger.warn("Failed to mount resource disk. "
                            "Attempting to format and retry mount. [{0}]",
                            output)

                shellutil.run(mkfs_string)
                ret, output = shellutil.run_get_output(mount_string)
                if ret:
                    raise Exception("Could not mount {0} "
                                            "after syncing partition table: "
                                            "[{1}] {2}".format(partition,
                                                                ret,
                                                                output))

        logger.info("Resource disk {0} is mounted at {1} with {2}",
                    device,
                    mount_point,
                    self.fs)
        return mount_point

    def device_for_lun(self, lunfile):
        """
        Return device name attached to a lun id'.
        """
        for folder, _, files in os.walk(AZURE_DISK_PATH):
            for f in files:
                if f == lunfile:
                    fp = os.path.join(folder,f)
                    if os.path.islink(fp):
                        return os.path.realpath(fp)
        return None

if __name__ == "__main__":
    print("Mounting data disk into {}".format(MOUNT_POINT)) 
    ddh = DataDiskHandler()
    ddh.run()