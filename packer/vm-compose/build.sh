#!/bin/bash
set -eu -o pipefail

# Installa ansible roles from Galaxy
ansible-galaxy install -r requirements.yml

# Build VM image
packer build -var-file=variables.json monitoring.json

echo "Image built successfully!"