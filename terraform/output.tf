output "terraform_remote_state" {
    description = "Terraform remote state file content"
    value = <<EOS

terraform {
  backend "azurerm" {
    resource_group_name  = "${var.resource_group_name}"
    storage_account_name = "${azurerm_storage_account.main.name}"
    container_name       = "${azurerm_storage_container.main.name}"
    key                  = "tfstate"
  }
}
EOS
}

output "resource_group_name" {
    description = "Azure resource group name"
    value = azurerm_resource_group.main.name
}

output "virtual_network_name" {
    description = "Azure Virtual Network name"
    value = azurerm_virtual_network.main.name
}

output "tls_root_ca_pem" {
  description   = "Root CA certificate"
  value         = module.root_tls_self_signed_ca.ca_cert_pem
}

output "tls_root_ca_key" {
  description   = "Root CA private key"
  value         = module.root_tls_self_signed_ca.ca_private_key_pem
  sensitive     = true
}

output "pki_name" {
  description   = "PKI name"
  value = var.pki_name
}

output "pki_common_name" {
  description   = "PKI common name"
  value = var.pki_common_name
}

output "pki_organization_name" {
  description   = "PKI organization name"
  value = var.pki_organization_name
}