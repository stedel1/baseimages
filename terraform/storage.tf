resource "azurerm_storage_account" "main" {
  name                     = "${var.prefix}storageaccount"
  resource_group_name      = azurerm_resource_group.main.name
  location                 = azurerm_resource_group.main.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  account_kind             = "StorageV2"
}

resource "azurerm_storage_container" "main" {
  name                  = "rg-shared-tfstate"
  storage_account_name  = azurerm_storage_account.main.name
  container_access_type = "private"
}