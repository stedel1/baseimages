variable "resource_group_name" {
    default = "rg-shared"
}

variable "prefix" {
  description   = "The prefix which should be used for all resources in this repo"
  default       = "orobixlab"
}

variable "location" {
  description   = "The Azure Region in which all resources in this example should be created."
  default       = "West Europe"
}

variable "address_space" {
  description   = "Azure virtual network IP address space"
  default       = "172.29.0.0/16"
}

# ---------------------------------------------------------------------------------------------------------------------
# pki Variables
# ---------------------------------------------------------------------------------------------------------------------

variable pki_name {
  description   = "PKI name"
  default       = "Orobix Dev"
}

variable pki_common_name {
  description   = "PKI common name "
  default       = "ROOT CA"
}

variable pki_organization_name {
  description   = "PKI organization name"
  default       = "Orobix s.r.l."
}