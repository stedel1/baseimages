# TODO: migrate to https://www.terraform.io/docs/providers/acme/r/certificate.html
module "root_tls_self_signed_ca" {
   source = "github.com/allxone/tls-self-signed-cert.git"

  name              = var.pki_name
  ca_common_name    = var.pki_common_name
  organization_name = var.pki_organization_name
  common_name       = var.pki_common_name
  download_certs    = "false"

  validity_period_hours = "87600" # 10 years
  early_renewal_hours = "720"     # 30 days
  
  rsa_bits = "2048"

  ca_allowed_uses = [
    "cert_signing",
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth",
  ]
}