resource "azurerm_virtual_network" "main" {
  name                = "${var.prefix}network"
  address_space       = [var.address_space]
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
}