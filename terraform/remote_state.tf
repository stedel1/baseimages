terraform {
  backend "azurerm" {
    resource_group_name  = "rg-shared"
    storage_account_name = "orobixlabstorageaccount"
    container_name       = "rg-shared-tfstate"
    key                  = "tfstate"
  }
}