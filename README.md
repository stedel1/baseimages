# Repo to manage rg-shared Azure Resource Group

- Terraform folder contains code to initialize rg-shared resource group and project related resource groups.

- Packer folder contains Hashicorp Packer projects to create Azure virtual machine images